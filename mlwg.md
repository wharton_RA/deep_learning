### Background
SemEval (Semantic Evaluation) is an ongoing series of evaluations of computational semantic analysis systems. It consists of challenges posed in conjunction with the annual \*SEM conference. In 2019 task 9 of SemEval was  to determine whether or not some text representing user or customer feedback contained a suggestion for improvement of the product or service in question. Making such determinations is termed "suggestion mining". I focused on this task because the data and some solutions are publicly available, and the solutions seem very relevant to our project of determining whether Wharton course evaluations contain suggestions for course improvement. 

Below is table that lists the top 10 teams in the challenge, their scores for the metric chosen by the challenge, an a description of their solution (the 8th ranked team is not listed because they did not publish their approach)
![rankings](rankings.png)

(source: https://www.aclweb.org/anthology/S19-2151.pdf)

The _F-Score_, or _F1_, is the harmonic mean of precision and recall calculations, and is the performance metric of choice for binary classification problems (classification problems with two classes - in our case "contains a suggestion" and "does not contain a suggestion"). "Precision" in this context the percentage of positive predictions that are true positives. "Recall" in this context is the percentage of true positives that are predicted to be such. 

F1 is useful for cases in which the training set is unbalanced - i.e. cases where one truth value is much more likely a priori than another. Suggestion mining is such a case because true positives (comments or reviews containing suggestions) occur far less frequently than true negatives (comments or reviews which do not). Accuracy, the metric we normally use in classification, is not useful in these cases because one can achieve reasonably high accuracy simply by making negative predictions.

In Subtask A, the models were evaluated on data from the same domain (a feedback entries from a forum for a software product) as the data on which they were trained. In Subtask B, the models were evaluated on data from a different domain (hotel reviews). An individual who appears to have been on the 10th ranking team was kind enough to make some of his exploratory data analysis public in a notebook on Kaggle (https://www.kaggle.com/sanketrai/suggestion-mining). The deep learning model in this notebook is based on that work. The major differences are:
1. I did not include parts of speech tags in the model input. I found it actually hindered the performance.
2. I included three other suggestion mining datasets that I found (tweets about Windows Phone 7,  online consumer electronics product reviews, and postings on an online political discussion forum)
3. I used a test set created by selecting (and removing) 10% of the instances from the combination of all the training datasets

The following code demonstrates training and evaluation of the model.

### Code


```python
# I'll skip the tedious process of downloading the datasets and transforming them into a standard format.
# I have the results of all that saved in a csv that I'll just load with pandas
import pandas as pd
all_train = pd.read_csv("all_data.csv")
```


```python
# Here is what the training data looks like
print(all_train)
```

                id                                           sentence  label
    0        663_3  "Please enable removing language code from the...      1
    1        663_4  "Note: in your .csproj file, there is a Suppor...      0
    2        664_1  "Wich means the new version not fully replaced...      0
    3        664_2  "Some of my users will still receive the old x...      0
    4        664_3  "The store randomly gives the old xap or the n...      0
    ...        ...                                                ...    ...
    20509  pr_1230  i had tried to uninstall norton internet secur...      0
    20510  pr_1231  i tried it again this morning and it said i di...      0
    20511  pr_1232  of course if you try to reach symantec custome...      0
    20512  pr_1233  finally i ran msconfig and went into the servi...      0
    20513  pr_1234  this time it started the uninstallation proces...      1
    
    [20514 rows x 3 columns]



```python
# First we separate the training data into inputs (x) and labels (y)
x_train = all_train['sentence'].values
y_train = all_train['label'].values
```


```python
# Next we use the keras library to create a tokenizer for the input
# All the tokenizer does at this stage is converd words to integers that represent their index in a dictionary
# that it creates
import keras
from keras.preprocessing import text, sequence
# max_features is the upper bound on the number of words we want in our dictionary.
# Only the 10,760 most frequent words will be in the dictionary. The rest will be encoded with a special 'unknown' token
max_features = 10760
# maxlen is the upper bound on the length of the text sequences we want to encode. 
# Longer sequences will be truncated.
maxlen = 600
tokenizer = text.Tokenizer(num_words = max_features)
tokenizer.fit_on_texts(list(x_train))
```

    Using TensorFlow backend.



```python
# Internally the tokenizer maintains a word index ordered by frequency. Let's take a look at the first few
list(tokenizer.word_index.keys())[:10]
```




    ['the', 'to', 'and', 'a', 'i', 'of', 'you', 'for', 'in', 'is']




```python
# Now we apply the tokenizer we created to our input
x_train = tokenizer.texts_to_sequences(x_train)
# We pad the input sequences to a fixed length since the model cannot accept variable length input
x_train = sequence.pad_sequences(x_train, maxlen = maxlen)
```


```python
# Let's take a look at how the first training instance is encoded
# This is what the text looks like
print(all_train.loc[0,'sentence'])
# This is what the encoding looks like (I did the last 100 tokens because its left-padded to the max sequence length)
print(x_train[0][-100:])
```

    "Please enable removing language code from the Dev Center "language history" For example if you ever selected "ru" and "ru-ru" laguages and you published this xap to the Store then it causes Tile localization to show the en-us(default) tile localization which is bad."
    [   0    0    0    0    0    0    0    0    0    0    0    0    0    0
        0    0    0    0    0    0    0    0    0    0    0    0    0    0
        0    0    0    0    0    0    0    0    0    0    0    0    0    0
        0    0    0    0    0    0    0    0    0    0    0    0   44  588
     2237  498  187   41    1  317  436  498  904    8  214   22    7  281
      969 1104    3 1104 1104 5803    3    7  991   16 1545    2    1  115
       90   12 1377  531 2238    2  220    1  552   92  521  531 2238   73
       10  272]



```python
# I have a dict mapping words to embeddings that I created from a fasttext word embedding data file saved as a pickle.
# I'll load it here.
import pickle
with open("embeddings.pkl","rb") as f:
    embeddings_index = pickle.load(f)
```


```python
# Let's take a peak at an entry in this dict
embeddings_index['the'][:10]
```




    array([ 0.0231,  0.017 ,  0.0157, -0.0773,  0.1088,  0.0031, -0.1487,
           -0.2672, -0.0357, -0.0487], dtype=float32)




```python
# I want to create an embedding matrix that I can use to look up a word embedding by its integer encoding.
# I'll initialize that here
import numpy as np
# This is the number of dimensions in a fasttext word embedding
embed_size = 300
# The shape of the matrix is (num words x embedding dimensions)
embedding_matrix = np.zeros((min(max_features, len(tokenizer.word_index)), embed_size))
```


```python
# Now we use the embeddings dict and the word index to populate the embedding matrix
# Each row represents an index in the word index. Each column represents a dimension in the word embedding for
# The word in the dictionary at that index.
for word, i in tokenizer.word_index.items():
    if i >= max_features:
        continue
    embedding_vector = embeddings_index.get(word)
    if embedding_vector is not None:
        embedding_matrix[i] = embedding_vector
```


```python
# Let's look at the beginning of a row in the embedding matrix
embedding_matrix[1][:10]
```




    array([ 0.0231    ,  0.017     ,  0.0157    , -0.0773    ,  0.1088    ,
            0.0031    , -0.1487    , -0.26719999, -0.0357    , -0.0487    ])




```python
# These are tensor-based versions of the binary classification metrics. A tensor is basically a numpy array that is able
# to be placed on a GPU. Tensorflow uses tensors as the data structure for input and output.
from sklearn.metrics import precision_score, recall_score, f1_score, accuracy_score
from keras import backend as K
def nn_recall(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + K.epsilon())
    return recall
def nn_precision(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    return precision
def nn_f1_score(y_true,y_pred):
    precision = nn_precision(y_true, y_pred)
    recall = nn_recall(y_true, y_pred)
    return 2*((precision*recall)/(precision+recall+K.epsilon()))
```


```python
# Here is the model definition.
# The model is based on the architecture proposed here: https://www.aclweb.org/anthology/S16-2022.pdf
# Model architecture seems like kind of a dark art. There are a number of different architectures that may be applied to
# NLP: CNN, RNN, etc. This model is a variation on the RNN - the recurrent neural network, which is considered
# particularly suited to encoding text sequences. I don't have a firm grasp on the function of each of the layers. There
# is plenty of interesting reading about it online, but it feels best to leave the details to the specialists.
# Suffice it to say the input is a batch of embedding sequences (128 x 10760 X 300) and the output is a batch of numbers
# between 0 and 1 representing the likelihood that the sequence contains a suggestion (128 x 1).
from keras.models import Model
from keras.layers import Input, Dense, Embedding,Flatten,SpatialDropout1D,Bidirectional,GRU,Conv1D, GlobalAveragePooling1D, GlobalMaxPooling1D, concatenate
inp = Input(shape = (maxlen, ))
x = Embedding(max_features, embed_size, weights = [embedding_matrix])(inp)
x = SpatialDropout1D(0.2)(x)
x = Bidirectional(GRU(100, return_sequences = True))(x)
x = Conv1D(50, kernel_size = 2, padding = "valid", kernel_initializer = "he_uniform")(x)
avg_pool = GlobalAveragePooling1D()(x)
max_pool = GlobalMaxPooling1D()(x)
conc = concatenate([avg_pool, max_pool])
outp = Dense(1,activation="sigmoid")(conc)
model = Model(inputs = inp, outputs = outp)
```

---
Sketchy diagram of model architecture taken from paper![lstm](lstm.png)


```python
# Keras requires that we 'compile the model'
# This basically creates an execution plan for training it.
# Binary cross-entropy is negative log likelihood on two classes, the standard loss function for binary classification 
# More here: https://ml-cheatsheet.readthedocs.io/en/latest/loss_functions.html
# The optimizer is the algorithm that adjusts the hyper-parameters of the training after each batch. The primary
# parameter is the learning rate (the coefficient applied to the gradient) and the basic idea is that the learning rate 
# gets slower as you train longer and the batch loss gets lower (you get smarter)
model.compile(loss = 'binary_crossentropy', optimizer = 'adam', metrics = ['accuracy',nn_f1_score])
```


```python
# Now we use a really cool function from scikit learn to take 10% of the data and create a test set.
from sklearn.model_selection import train_test_split
x_train, x_dev, y_train, y_dev = train_test_split(x_train, y_train, test_size=0.1, random_state=12)
```


```python
# Now we actually train the model. The batch size is how many input examples are fed into the model at once.
# Loss is calculated for the entire batch and that loss is used for the gradient. The batching technique not only speeds
# Up execution time (especially when a GPU is present), but also prevents over-fitting.
# An epoch is one pass over the entire training set. With these inputs an epoch takes about 10 minutes
model.fit(x_train, y_train, batch_size = 128, epochs = 1, validation_data = (x_dev, y_dev), verbose = 1)
```

    /Users/jamtrac/.local/lib/python3.7/site-packages/tensorflow_core/python/framework/indexed_slices.py:433: UserWarning: Converting sparse IndexedSlices to a dense Tensor of unknown shape. This may consume a large amount of memory.
      "Converting sparse IndexedSlices to a dense Tensor of unknown shape. "


    Train on 18462 samples, validate on 2052 samples
    Epoch 1/1
    18462/18462 [==============================] - 501s 27ms/step - loss: 0.4289 - accuracy: 0.7967 - nn_f1_score: 0.4563 - val_loss: 0.3376 - val_accuracy: 0.8475 - val_nn_f1_score: 0.6805





    <keras.callbacks.callbacks.History at 0x13a550d10>




```python
# Let's print out some metrics using our test dataset
nn_dev_pred = model.predict(x_dev, batch_size = 128, verbose = 1)
y_pred = (nn_dev_pred >= 0.5).astype(int)
nn_precision = precision_score(y_dev, y_pred)
nn_recall = recall_score(y_dev, y_pred)
nn_f1 = f1_score(y_dev, y_pred)
nn_acc = accuracy_score(y_dev,y_pred)
print(f"Precision score: {(nn_precision):0.3f}")
print(f"Recall score: {(nn_recall):0.3f}")
print(f"F1 score: {(nn_f1):0.3f}")
print(f"Accuracy score: {(nn_acc):0.3f}")
print(f"Percent true positive: {(K.sum(y_dev) / y_dev.shape[0]):0.3f}")
```

    2052/2052 [==============================] - 12s 6ms/step
    Precision score: 0.793
    Recall score: 0.572
    F1 score: 0.665
    Accuracy score: 0.847
    Percent true positive: 0.264



```python
# Not too bad! This is competitive with the other Semeval Task 9 entries, but I guess we should have expected
# that since we "borrowed" it from one of the teams. It's notable that it only took 10 minutes to train on a latop with
# no GPU and that it's robust to the addition of data from different domains to the training set. Now let's load some 
# data from a domain not at all present in the training set: the coursera data that we labeled
# in our Sagemaker ground truth job. Again, I'm not going to go through the tedious details of formatting it.
# I'll just load it from a csv.
coursera = pd.read_csv("coursera.csv")
```


```python
# We encode the tokens first using the same tokenizer we used on the training set
x_test = coursera['sentence'].values
x_test = tokenizer.texts_to_sequences(x_test)
x_test = sequence.pad_sequences(x_test, maxlen = maxlen)
```


```python
# Make sure to grab the labels as well for later
y_test = coursera['label'].values
y_test = y_test.astype(np.int)
```


```python
# Evaluate the model on the test set
nn_dev_pred = model.predict(x_test, batch_size = 128, verbose = 1)
y_pred = (nn_dev_pred >= 0.5).astype(int)
nn_precision = precision_score(y_test, y_pred)
nn_recall = recall_score(y_test, y_pred)
nn_f1 = f1_score(y_test, y_pred)
nn_acc = accuracy_score(y_test,y_pred)
print(f"Precision score: {(nn_precision):0.3f}")
print(f"Recall score: {(nn_recall):0.3f}")
print(f"F1 score: {(nn_f1):0.3f}")
print(f"Accuracy score: {(nn_acc):0.3f}")
print(f"Percent true positive: {(K.sum(y_test) / y_test.shape[0]):0.3f}")
```

    2026/2026 [==============================] - 12s 6ms/step
    Precision score: 0.317
    Recall score: 0.306
    F1 score: 0.312
    Accuracy score: 0.885
    Percent true positive: 0.085


### Conclusion
Training a suggestion miner on data from other domains and then applying it to course evaluations is not likely to work. However, the good performance of our model on new domains that *were* included in the training dataset suggests that if we have a large enough labeled set of course reviews to include in the training data, we might have some success. So that could be a next step. 

I'd also like to note that all of the solutions higher than the LSTM solution in the rankings where some variation on a transfer from a pretrained model (specifically BERT - Bidirectional Encoder Representations from Transformers). Transfer learning attempts to go beyond the idea of word embeddings by replacing entire sentences hidden states from deep learning models trained to predict the next tokens of sequences in large corpuses. The idea is that these hidden states, like embeddings, represent useful features of the input sequence. I made every effort to create and "fine-tune" a BERT-based transformer using the tools here https://github.com/huggingface/transformers, but ultimately couldn't get it to work. This could also be an interesting next step for this project, but it is likely to require some amount of time on a machine with GPU capabilities.


```python

```
